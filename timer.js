var timerStart = self.addEventListener("message", function(e) {
	if(e.data.startTimer) {
		startTimer();
	}
	if(e.data.stopTimer) {
		stopTimer();
	}
}, false);

var timerInterval = null;

function myTimer(d0)
{
	// get current time
	var d = (new Date()).valueOf();
	var startValue = 60000;
	var diff = startValue - (d - d0);
	var seconds = Math.floor(diff / 1000);

	seconds = seconds.toString();
	if (seconds.length == 1) {
		seconds = "0" + seconds;
	}

	// return output to Web Worker
	postMessage(seconds);
}

function startTimer() {

	// get current time
	var d0 = (new Date()).valueOf();

	if(timerInterval !== null) {
		clearInterval(timerInterval);
	}

	timerInterval = setInterval(function() {
		myTimer(d0);
	}, 100);
}

function stopTimer() {
	return clearInterval(timerInterval);
}
